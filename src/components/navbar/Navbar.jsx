import React, {useState} from 'react'
import "../navbar/Navbar.css"
import {RiMenuLine,RiCloseCircleLine} from "react-icons/ri"
import logo from '../../assets/logo.svg'


// const Menu = () => {
//   <>     
//     <p><a href="#home">Home</a></p>
//     <p><a href="#wgpt3">What is GPT</a></p>
//     <p><a href="#possibilitiy">Open AI</a></p>
//     <p><a href="#features">Case Studies</a></p>
//     <p><a href="#blog">Library</a></p>
// </>




const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false)
  return (
    <div className='gpt3_navbar'>
         <div className='gpt3_navbar-links'> 
              <div className='gpt3_navbar-links_logo'>
                        <img src={logo} alt="logo" />
                  </div>
                  <div className='gpt3_navbar-links_container'>
                  <p><a href="#home">Home</a></p> 
                    <p><a href="#wgpt3">What is GPT</a></p>
                    <p><a href="#possibilitiy">Open AI</a></p>
                    <p><a href="#features">Case Studies</a></p>
                    <p><a href="#blog">Library</a></p>
                
                   
              </div>
           </div>
     


           <div className='gpt3_navbar-sign'>
                  <p>Sign in</p>
                  <button className='button'> SignUp</button>
           </div>


           <div className='gpt3_navbar-menu'>
                  {toggleMenu ? <RiCloseCircleLine color="#fff" size={27} onClick={() =>setToggleMenu(false)}/>
                        :  <RiMenuLine color="#fff" size={27} onClick={() =>setToggleMenu(true)}/>}

                        {toggleMenu && ( <div className="gpt3_navbar-menu_container"> 
                                  <div className="gpt3_navbar-menu_container-links">
                                          {/* <Menu/> */}
                                          <p><a href="#home">Home</a></p>
                                              <p><a href="#wgpt3">What is GPT</a></p>
                                              <p><a href="#possibilitiy">Open AI</a></p>
                                              <p><a href="#features">Case Studies</a></p>
                                              <p><a href="#blog">Library</a></p>
                                          <div className='gpt3_navbar-menu_container-links-sign'>
                                                <p>Sign in</p>
                                                  <button className='button'>Sign Up</button>
                                          </div>
                                    
                                  </div>
                        </div> 
                            
                          
                        )}
           </div>
    </div>
  )
}

export default Navbar