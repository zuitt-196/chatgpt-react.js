// IMPORT FROM COMPONENTS
import { Navbar, Cta,Brand  } from "./components";
import './App.css'

// IMPOORT FROM CONTAINER
import {Blog,Features ,Footer, Possibility, WhatGTP3 ,Header} from "./containers"



function App() { 
  return (
    <div className="App"> 
        <div className="gradient_bg">
            <Navbar/>
            <Header/>
        </div>
        <Brand/>
          <WhatGTP3/>
          <Features/>
          <Possibility/>
          <Cta/>
          <Blog/>
          <Footer/>
       </div>
  );
}

export default App;
